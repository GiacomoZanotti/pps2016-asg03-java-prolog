package u07asg

import java.io.FileInputStream
import java.util.{Optional, OptionalInt}

import alice._
import alice.tuprolog.{Struct, Theory}
import u07asg.AI.Cell
import u07asg.Scala2P.{solveWithSuccess, _}

import scala.collection.JavaConverters._
import scala.collection.mutable.Buffer

/**
  * Created by mirko on 4/10/17.
  */
class TicTacToeImpl(fileName: String) extends TicTacToe {

  implicit private def playerToString(player: Player): String = player match {
    case Player.PlayerX => "p1"
    case _ => "p2"
  }

  implicit private def stringToPlayer(s: String): Player = s match {
    case "p1" => Player.PlayerX
    case _ => Player.PlayerO
  }

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
  createBoard()

  override def createBoard() = {
    val goal = "retractall(board(_)),create_board(B),assert(board(B))"
    solveWithSuccess(engine, goal)
  }

  override def getBoard() = {
    val term = solveOneAndGetTerm(engine, "board(B)", "B").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map {
      case "null" => Optional.empty[Player]()
      case s => Optional.of[Player](s)
    }.to[Buffer].asJava
  }

  override def checkCompleted() = {
    val goal = "board(B),final(B,_)"
    solveWithSuccess(engine, goal)
  }

  override def checkVictory() = {
    val goal = "board(B),final(B,p1)"
    val goal2 = "board(B),final(B,p2)"
    if (solveWithSuccess(engine, goal)) {
      Optional.of(Player.PlayerX)
    }
    else if (solveWithSuccess(engine, goal2)) {

      Optional.of(Player.PlayerO)
    }
    else Optional.empty()
  }

  override def move(player: Player, i: Int, j: Int): Boolean = {
    val goal = s"board(B), next_board(B,${playerToString(player)},B2)"
    val nextboard = (for {
      term <- engine(goal).map(extractTerm(_, "B2"))
      elem = term.asInstanceOf[Struct].listIterator().asScala.toList(i + 3 * j)
      if elem.toString == playerToString(player)
    } yield term).headOption
    if (nextboard isEmpty) return false
    val goal2 = s"retractall(board(_)), assert(board(${nextboard.get.toString}))"
    solveWithSuccess(engine, goal2)
  }

  override def toString =
    solveOneAndGetTerm(engine, "board(B)", "B").toString

  override def winCount(current: Player, winner: Player): Int = {
    val goal = s"board(B), statistics(B,${playerToString(current)},${playerToString(winner)},Count)"
    solveOneAndGetTerm(engine, goal, "Count").asInstanceOf[tuprolog.Int].intValue()
  }


  override def AIAboutToWIn() = {
    val goal = "board(B),isabouttowin(B,p2,P,IndexRow,IndexColumn)"
    if (solveWithSuccess(engine, goal)) {

      val index_row = OptionalInt.of(solveOneAndGetTerm(engine, goal, "IndexRow").asInstanceOf[tuprolog.Int].intValue()).getAsInt
      val index_column = OptionalInt.of(solveOneAndGetTerm(engine, goal, "IndexColumn").asInstanceOf[tuprolog.Int].intValue()).getAsInt
      val cell: Optional[Cell] = Optional.of(new Cell(index_row, index_column))
      cell
    }
    else {
      Optional.empty()
    }
  }

  override def HumanAboutToWIn() = {
    val goal = "board(B),isabouttowin(B,p1,P,IndexRow,IndexColumn)"


    if ( solveWithSuccess(engine, goal)) {

      val index_row = OptionalInt.of(solveOneAndGetTerm(engine, goal, "IndexRow").asInstanceOf[tuprolog.Int].intValue()).getAsInt
      val index_column = OptionalInt.of(solveOneAndGetTerm(engine, goal, "IndexColumn").asInstanceOf[tuprolog.Int].intValue()).getAsInt

      val cell: Optional[Cell] = Optional.of(new Cell(index_row, index_column))
     solveWithSuccess(engine, s"retract(almostwinpatter(P,$index_row,$index_column))")


      cell
    }
    else {
      Optional.empty()
    }
  }


}