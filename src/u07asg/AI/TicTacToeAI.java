package u07asg.AI;


import java.util.Optional;
import java.util.OptionalInt;

/**
 * Created by OPeratore on 27/04/2017.
 */
public interface TicTacToeAI {

    Optional<Cell> makeAMove();

    void specificMove(Cell cell);



}
