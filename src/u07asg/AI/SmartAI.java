package u07asg.AI;

import javax.swing.*;
import java.util.Optional;
import java.util.Random;

/**
 * Created by OPeratore on 30/04/2017.
 */
public class SmartAI implements TicTacToeAI {

    private JButton[][] board;
    private final String SYMBOL = "O";
    private Random r;

    public SmartAI(JButton[][] board) {
        r = new Random();

        this.board = board;
    }


    @Override
    public Optional<Cell> makeAMove() {
        int index_row = 0;
        int index_column = 0;

        while (!board[index_row][index_column].getText().equals("")) {
            index_row = r.nextInt(3);
            index_column = r.nextInt(3);
        }
        board[index_row][index_column].setText(SYMBOL);
        return Optional.of(new Cell(index_row, index_column));

    }


    @Override
    public void specificMove(Cell cell) {
        int index_row = cell.getIndex_row();
        int index_column = cell.getIndex_column();
        board[index_row][index_column].setText(SYMBOL);



    }


}
