package u07asg.AI;

/**
 * Created by OPeratore on 28/04/2017.
 */
public class Cell {
    private int index_row;
    private int index_column;

    public Cell(int index_row, int index_column) {
        this.index_row = index_row;
        this.index_column = index_column;
    }

    public int getIndex_row() {
        return index_row;
    }

    public int getIndex_column() {
        return index_column;
    }
    @Override
    public String toString(){
        return "("+index_row+","+index_column+")";
    }
}
