package u07asg;


import u07asg.AI.Cell;
import u07asg.AI.SmartAI;
import u07asg.AI.TicTacToeAI;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

import static jdk.nashorn.internal.objects.Global.objectPrototype;
import static jdk.nashorn.internal.objects.Global.println;

/**
 * A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[3][3];
    private final JButton exit = new JButton("Exit");

    private TicTacToeAI smartAI;
    private final JFrame frame = new JFrame("TTT");
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;


    private void changeTurn() {
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt = ttt;
        smartAI = new SmartAI(board);
        initPane();
    }

    private void machineMove() {
        Optional<Cell> checkCellForAI = ttt.AIAboutToWIn();
        Optional<Cell> checkCellForHuman = ttt.HumanAboutToWIn();

        Optional<Cell> cell;

        if (checkCellForAI.isPresent() && ttt.move(turn, checkCellForAI.get().getIndex_row(), checkCellForAI.get().getIndex_column())) {
            smartAI.specificMove(checkCellForAI.get());
            System.out.println("I'm tryng to win! " + checkCellForAI.get().toString());


        } else if (checkCellForHuman.isPresent() && ttt.move(turn, checkCellForHuman.get().getIndex_row(), checkCellForHuman.get().getIndex_column())) {
            smartAI.specificMove(checkCellForHuman.get());
            System.out.println("I'm blocking! " + checkCellForHuman.get().toString());

        } else {
            System.out.println("random move ");
            cell = smartAI.makeAMove();
            cell.ifPresent(cell1 -> ttt.move(turn, cell.get().getIndex_row(), cell.get().getIndex_column()));

        }
        moves++;
        changeTurn();
        if (moves > 3) {
            System.out.println("O winning count: " + ttt.winCount(turn, Player.PlayerO));
        }
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()) {
            exit.setText(victory.get() + " won!");
            finished = true;
            return;
        }


    }

    private void humanMove(int i, int j) {
        if (ttt.move(turn, i, j)) {
            board[i][j].setText("X");
            moves++;
            changeTurn();
            if (moves > 3) {
                System.out.println("X winning count: " + ttt.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: " + ttt.winCount(turn, Player.PlayerO));
            }
        }
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()) {
            exit.setText(victory.get() + " won!");
            finished = true;
            return;
        }
        if (ttt.checkCompleted()) {
            exit.setText("Even!");
            finished = true;
            return;
        }
    }

    private void initPane() {
        frame.setLayout(new BorderLayout());
        JPanel b = new JPanel(new GridLayout(3, 3));
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                final int i2 = i;
                final int j2 = j;
                board[i][j] = new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> {
                    if (!finished && turn == Player.PlayerX) {
                        humanMove(i2, j2);
                    }
                    if (!finished && turn == Player.PlayerO) {
                        machineMove();
                    }
                });
            }
        }
        JPanel s = new JPanel(new FlowLayout());
        s.add(exit);

        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER, b);
        frame.add(BorderLayout.SOUTH, s);
        frame.setSize(300, 230);

        frame.setVisible(true);
    }

    public static void main(String[] args) {
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
