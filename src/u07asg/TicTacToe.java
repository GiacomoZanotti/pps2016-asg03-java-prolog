package u07asg;

import u07asg.AI.Cell;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

enum Player {PlayerX, PlayerO}

public interface TicTacToe {

    void createBoard();

    Optional<Cell> AIAboutToWIn();

    Optional<Cell> HumanAboutToWIn();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);
}
